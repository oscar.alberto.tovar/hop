# syntax=docker/dockerfile:1
FROM docker.io/library/golang:1.20 as BUILDER

WORKDIR /workspace

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN <<EOF
go version
mkdir -p bin/
CGO_ENABLED=0 go build -a -installsuffix cgo -o bin/hop ./cmd/hop/
EOF

FROM scratch

COPY --from=BUILDER /workspace/bin/hop /

ENTRYPOINT [ "/hop" ]
