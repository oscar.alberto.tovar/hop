package command

// Command represents a redirection command.
type Command struct {
	Name        string
	Description string
	BaseURL     string
	Redirect    func(args []string) (string, error)
	Subcommands []*Command
	Args        []*Arg
}

type Arg struct {
	Name        string
	Description string
}

// AddSubcommand adds a subcommand to the the command.
func (c *Command) AddSubcommand(cmd *Command) {
	if cmd == nil {
		return
	}
	c.Subcommands = append(c.Subcommands, cmd)
}

// Run executes the command with the given args and returns a url to
// redirect to if error is nil.
func (c *Command) Run(args []string) (string, error) {
	if len(args) == 0 && c.BaseURL != "" {
		return c.BaseURL, nil
	}

	for _, cmd := range c.Subcommands {
		if cmd.Name == args[0] {
			return cmd.Run(args[1:])
		}
	}

	return c.Redirect(args)
}
