package command

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCommand_AddSubcommand(t *testing.T) {
	t.Parallel()
	cmd := Command{Name: "command"}
	subcommand := Command{Name: "subcommand"}
	cmd.AddSubcommand(&subcommand)
	assert.Equal(t, cmd.Subcommands, []*Command{&subcommand})
}
