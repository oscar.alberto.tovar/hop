package reddit

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCommand(t *testing.T) {
	tests := map[string]struct {
		args   []string
		expect string
	}{
		"no args": {
			args:   nil,
			expect: "https://www.reddit.com/",
		},
		"single argument": {
			args:   strings.Fields("subreddit"),
			expect: "https://www.reddit.com/r/subreddit",
		},
		"multiple arguments": {
			args:   strings.Fields("subreddit random args here"),
			expect: "https://www.reddit.com/r/subreddit",
		},
	}
	for name := range tests {
		tt := tests[name]
		t.Run(name, func(t *testing.T) {
			t.Parallel()
			got, err := Command.Run(tt.args)
			require.NoError(t, err)
			assert.Equal(t, tt.expect, got)
		})
	}
}
