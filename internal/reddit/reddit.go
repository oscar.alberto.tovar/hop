package reddit

import (
	"net/url"

	"gitlab.com/oscar.alberto.tovar/hop/internal/command"
)

const baseURL = "https://www.reddit.com/"

var Command = &command.Command{
	Name:        "r",
	Description: "Jump directly to a subreddit.",
	BaseURL:     baseURL,
	Redirect: func(args []string) (string, error) {
		if len(args) == 0 {
			return baseURL, nil
		}
		subreddit := args[0]
		return baseURL + "r/" + url.PathEscape(subreddit), nil
	},
}
