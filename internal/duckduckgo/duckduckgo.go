package duckduckgo

import (
	"net/url"
	"strings"

	"gitlab.com/oscar.alberto.tovar/hop/internal/command"
)

const baseURL = "https://www.duckduckgo.com/"

var Command = &command.Command{
	Name:        "ddg",
	Description: "Search on duckduckgo.",
	BaseURL:     baseURL,
	Redirect: func(args []string) (string, error) {
		if len(args) == 0 {
			return baseURL, nil
		}

		q := strings.Join(args, " ")
		return baseURL + "?q=" + url.QueryEscape(q), nil
	},
	Subcommands: nil,
}
