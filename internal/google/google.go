package google

import (
	"net/url"
	"strings"

	"gitlab.com/oscar.alberto.tovar/hop/internal/command"
)

const baseURL = "https://www.google.com"

var GSearchCommand = &command.Command{
	Name:        "g",
	Description: "Search on Google.",
	BaseURL:     baseURL,
	Redirect: func(args []string) (string, error) {
		if len(args) == 0 {
			return baseURL, nil
		}

		q := strings.Join(args, " ")
		return baseURL + "/search?q=" + url.QueryEscape(q), nil
	},
	Subcommands: nil,
}

var GMailCommand = &command.Command{
	Name:        "gmail",
	Description: "Browse and search your Gmail.",
	BaseURL:     "https://mail.google.com/",
	Redirect: func(args []string) (string, error) {
		if len(args) == 0 {
			return "https://mail.google.com/", nil
		}
		q := strings.Join(args, " ")
		return "https://mail.google.com/#search/" + url.QueryEscape(q), nil
	},
	Subcommands: nil,
}

var GDocsCommand = &command.Command{
	Name:        "gdocs",
	Description: "Browse and search your Google Docs.",
	BaseURL:     "https://docs.google.com/document",
	Redirect: func(args []string) (string, error) {
		if len(args) == 0 {
			return "https://docs.google.com/document", nil
		}
		q := strings.Join(args, " ")
		return "https://docs.google.com/document/?q=" + url.QueryEscape(q), nil
	},
}

var GSheetsCommand = &command.Command{
	Name:        "gsheets",
	Description: "Browse and search your Google Sheets.",
	BaseURL:     "https://docs.google.com/spreadsheets",
	Redirect: func(args []string) (string, error) {
		if len(args) == 0 {
			return "https://docs.google.com/spreadsheets", nil
		}
		q := strings.Join(args, " ")
		return "https://docs.google.com/spreadsheets/?q=" + url.QueryEscape(q), nil
	},
}

var GSlidesCommand = &command.Command{
	Name:        "gslides",
	Description: "Browse and search your Google Slides.",
	BaseURL:     "https://docs.google.com/presentations",
	Redirect: func(args []string) (string, error) {
		if len(args) == 0 {
			return "https://docs.google.com/presentations", nil
		}
		q := strings.Join(args, " ")
		return "https://docs.google.com/presentations/?q=" + url.QueryEscape(q), nil
	},
}
