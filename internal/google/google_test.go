package google

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGSearchCommand(t *testing.T) {
	tests := map[string]struct {
		args   []string
		expect string
	}{
		"no arguments": {
			args:   nil,
			expect: "https://www.google.com",
		},
		"multiple arguments": {
			args:   strings.Fields("where is the nearest coffee shop?"),
			expect: "https://www.google.com/search?q=where+is+the+nearest+coffee+shop%3F",
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := GSearchCommand.Run(tt.args)
			assert.NoError(t, err)
			assert.Equal(t, tt.expect, got)
		})
	}
}

func TestGmailCommand(t *testing.T) {
	tests := map[string]struct {
		args   []string
		expect string
	}{
		"no arguments": {
			args:   nil,
			expect: "https://mail.google.com/",
		},
		"multiple arguments": {
			args:   strings.Fields("where is the nearest coffee shop?"),
			expect: "https://mail.google.com/#search/where+is+the+nearest+coffee+shop%3F",
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := GMailCommand.Run(tt.args)
			assert.NoError(t, err)
			assert.Equal(t, tt.expect, got)
		})
	}
}

func TestGDocsCommand(t *testing.T) {
	tests := map[string]struct {
		args   []string
		expect string
	}{
		"no arguments": {
			args:   nil,
			expect: "https://docs.google.com/document",
		},
		"multiple arguments": {
			args:   strings.Fields("where is the nearest coffee shop?"),
			expect: "https://docs.google.com/document/?q=where+is+the+nearest+coffee+shop%3F",
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := GDocsCommand.Run(tt.args)
			assert.NoError(t, err)
			assert.Equal(t, tt.expect, got)
		})
	}
}

func TestGSheetsCommand(t *testing.T) {
	tests := map[string]struct {
		args   []string
		expect string
	}{
		"no arguments": {
			args:   nil,
			expect: "https://docs.google.com/spreadsheets",
		},
		"multiple arguments": {
			args:   strings.Fields("where is the nearest coffee shop?"),
			expect: "https://docs.google.com/spreadsheets/?q=where+is+the+nearest+coffee+shop%3F",
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := GSheetsCommand.Run(tt.args)
			assert.NoError(t, err)
			assert.Equal(t, tt.expect, got)
		})
	}
}

func TestGSlidesCommand(t *testing.T) {
	tests := map[string]struct {
		args   []string
		expect string
	}{
		"no arguments": {
			args:   nil,
			expect: "https://docs.google.com/presentations",
		},
		"multiple arguments": {
			args:   strings.Fields("where is the nearest coffee shop?"),
			expect: "https://docs.google.com/presentations/?q=where+is+the+nearest+coffee+shop%3F",
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := GSlidesCommand.Run(tt.args)
			assert.NoError(t, err)
			assert.Equal(t, tt.expect, got)
		})
	}
}
