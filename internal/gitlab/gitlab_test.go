package gitlab

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCommand_ValidArgs(t *testing.T) {
	tests := map[string]struct {
		args   []string
		expect string
	}{
		"no args": {
			args:   nil,
			expect: "https://gitlab.com",
		},
		"create with no subcommand": {
			args:   []string{"create"},
			expect: "https://gitlab.com",
		},
		"create issue with project arg": {
			args:   []string{"create", "issue", "user.with.dots/project_with_underscores"},
			expect: "https://gitlab.com/user.with.dots/project_with_underscores/-/issues/new",
		},
		"create snippet with project arg": {
			args:   []string{"create", "snippet", "user.with.dots/project_with_underscores"},
			expect: "https://gitlab.com/user.with.dots/project_with_underscores/-/snippets/new",
		},
		"get with no subcommand": {
			args:   []string{"get"},
			expect: "https://gitlab.com",
		},
		"get snippet": {
			args:   []string{"get", "snippet"},
			expect: "https://gitlab.com/dashboard/snippets",
		},
		"profile": {
			args:   []string{"profile"},
			expect: "https://gitlab.com",
		},
		"profile with username": {
			args:   []string{"profile", "firstname.lastname"},
			expect: "https://gitlab.com/firstname.lastname",
		},
	}
	for name := range tests {
		tt := tests[name]
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got, err := Command.Run(tt.args)
			assert.NoError(t, err)
			assert.Equal(t, tt.expect, got)
		})
	}
}

func TestCommand_InvalidArgs(t *testing.T) {
	tests := map[string]struct {
		args []string
	}{
		"create issue": {
			args: []string{"create", "issue"},
		},
	}
	for name := range tests {
		tt := tests[name]
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got, err := Command.Run(tt.args)
			assert.Error(t, err)
			assert.Empty(t, got)
		})
	}
}
