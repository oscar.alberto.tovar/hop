package gitlab

import (
	"fmt"

	"github.com/MakeNowJust/heredoc"

	"gitlab.com/oscar.alberto.tovar/hop/internal/command"
)

const (
	baseURL = "https://gitlab.com"
)

var Command = &command.Command{
	Name:        "glab",
	Description: "Perform various tasks on GitLab.com.",
	BaseURL:     baseURL,
	Redirect: func(args []string) (string, error) {
		return baseURL, nil
	},
	Subcommands: []*command.Command{
		profile,
		todos,
		create,
		get,
	},
}

var profile = &command.Command{
	Name:        "profile",
	Description: "View an accounts profile.",
	BaseURL:     baseURL,
	Redirect:    func(args []string) (string, error) { return baseURL + "/" + args[0], nil },
}

var todos = &command.Command{
	Name:        "todos",
	Description: "View items on your ToDo list.",
	BaseURL:     baseURL + "/dashboard/todos",
	Redirect:    func(args []string) (string, error) { return baseURL + "/dashboard/todos", nil },
	Subcommands: nil,
}

var get = &command.Command{
	Name:        "get",
	Description: "Get issues, snippets, and merge requests",
	BaseURL:     baseURL,
	Redirect:    func(args []string) (string, error) { return baseURL, nil },
	Subcommands: []*command.Command{
		{
			Name:        "snippet",
			Description: "List snippets",
			BaseURL:     baseURL + "/dashboard/snippets",
			Redirect: func(args []string) (string, error) {
				return baseURL + "/dashboard/snippets", nil
			},
			Subcommands: nil,
			Args:        nil,
		},
	},
}

var create = &command.Command{
	Name:        "create",
	Description: "Create issues, snippets and merge requests",
	BaseURL:     baseURL,
	Redirect:    func(args []string) (string, error) { return baseURL, nil },
	Subcommands: []*command.Command{
		{
			Name:        "issue",
			Description: "Create a new issue.",
			BaseURL:     "",
			Redirect: func(args []string) (string, error) {
				if len(args) == 0 {
					return "", fmt.Errorf("project arg missing")
				}
				project := args[0]
				return baseURL + "/" + project + "/-/issues/new", nil
			},
			Subcommands: nil,
			Args: []*command.Arg{
				{
					Name: "project",
					Description: heredoc.Doc(`
					The path to the project relative to https://gitlab.com.
					For example, you would use dev-group/web-app if you're trying
					to create an issue on the https://gitlab.com/dev-group/web-app
					project.
					`),
				},
			},
		},
		{
			Name:        "snippet",
			Description: "Create a new snippet.",
			BaseURL:     baseURL + "/-/snippets/new",
			Redirect: func(args []string) (string, error) {
				if len(args) == 0 {
					return baseURL + "/-/snippets/new", nil
				}

				project := args[0]
				return baseURL + "/" + project + "/-/snippets/new", nil
			},
			Subcommands: nil,
		},
	},
}
