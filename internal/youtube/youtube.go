package youtube

import (
	"net/url"
	"strings"

	"gitlab.com/oscar.alberto.tovar/hop/internal/command"
)

const baseURL = "https://www.youtube.com/"

var Command = &command.Command{
	Name:        "yt",
	Description: "Browse and search on YouTube.",
	BaseURL:     baseURL,
	Redirect: func(args []string) (string, error) {
		if len(args) == 0 {
			return baseURL, nil
		}
		q := strings.Join(args, " ")
		return baseURL + "results?search_query=" + url.QueryEscape(q), nil
	},
}
