package youtube

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCommand(t *testing.T) {
	tests := map[string]struct {
		args   []string
		expect string
	}{
		"no args": {
			args:   nil,
			expect: "https://www.youtube.com/",
		},
		"multiple arguments": {
			args:   strings.Fields("where is the nearest coffee shop?"),
			expect: "https://www.youtube.com/results?search_query=where+is+the+nearest+coffee+shop%3F",
		},
	}
	for name := range tests {
		tt := tests[name]
		t.Run(name, func(t *testing.T) {
			t.Parallel()
			got, err := Command.Run(tt.args)
			require.NoError(t, err)
			assert.Equal(t, tt.expect, got)
		})
	}
}
