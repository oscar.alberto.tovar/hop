package router

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"

	"gitlab.com/oscar.alberto.tovar/hop/internal/command"
)

type CommandService struct {
	commands map[string]*command.Command
}

func NewCommandService(cmds ...*command.Command) (*CommandService, error) {
	s := &CommandService{
		commands: make(map[string]*command.Command),
	}
	for _, cmd := range cmds {
		if err := s.addCommand(cmd); err != nil {
			return nil, err
		}
	}
	return s, nil
}

func (s *CommandService) addCommand(cmd *command.Command) error {
	if _, ok := s.commands[cmd.Name]; ok {
		return fmt.Errorf("command with name %q already registered", cmd.Name)
	}
	s.commands[cmd.Name] = cmd
	return nil
}

func (s *CommandService) Handler(c echo.Context) error {
	cmdStr := c.QueryParam("cmd")
	args := strings.Fields(cmdStr)
	if len(args) == 0 {
		return c.Redirect(http.StatusPermanentRedirect, "/")
	}
	cmd, ok := s.commands[args[0]]
	if !ok {
		return c.String(http.StatusBadRequest, fmt.Sprintf("command %q not found", args[0]))
	}

	redirect, err := cmd.Run(args[1:])
	if err != nil {
		return err
	}
	return c.Redirect(http.StatusPermanentRedirect, redirect)
}
