package router_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/oscar.alberto.tovar/hop/internal/duckduckgo"
	"gitlab.com/oscar.alberto.tovar/hop/internal/google"
	"gitlab.com/oscar.alberto.tovar/hop/internal/router"
)

func TestNewCommandService(t *testing.T) {
	t.Run("unique commands registered", func(t *testing.T) {
		t.Parallel()

		got, err := router.NewCommandService(google.GSearchCommand, duckduckgo.Command)
		assert.NoError(t, err)
		assert.NotNil(t, got)
	})

	t.Run("non-unique commands registered", func(t *testing.T) {
		t.Parallel()

		got, err := router.NewCommandService(google.GSearchCommand, google.GSearchCommand)
		assert.Error(t, err)
		assert.Nil(t, got)
	})
}
