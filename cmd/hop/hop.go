package main

import (
	"log"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"gitlab.com/oscar.alberto.tovar/hop/internal/duckduckgo"
	"gitlab.com/oscar.alberto.tovar/hop/internal/gitlab"
	"gitlab.com/oscar.alberto.tovar/hop/internal/google"
	"gitlab.com/oscar.alberto.tovar/hop/internal/reddit"
	"gitlab.com/oscar.alberto.tovar/hop/internal/router"
	"gitlab.com/oscar.alberto.tovar/hop/internal/youtube"
)

func main() {
	cmdService, err := router.NewCommandService(
		duckduckgo.Command,
		gitlab.Command,
		google.GSearchCommand,
		google.GDocsCommand,
		google.GMailCommand,
		google.GSheetsCommand,
		google.GSlidesCommand,
		reddit.Command,
		youtube.Command,
	)
	if err != nil {
		log.Fatalf("Creating command service: %s", err)
	}

	e := echo.New()
	e.Use(
		middleware.Decompress(),
		middleware.Gzip(),
		middleware.Logger(),
	)
	e.GET("/hop", cmdService.Handler)
	if err := e.Start(":3000"); err != nil {
		log.Fatalf("Starting web server: %s", err)
	}
}
