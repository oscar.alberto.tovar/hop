# Command ⌨️

<!--
Describe how the new command will behave (including subcommands).
-->

```plaintext
g <search term>

g what is the coffee shop nearest to me?
```

## Plan 📃

<!--
Write the plan needed to implement the command.
-->

## Validation 🧪

<!--
Post a screenshot of how this works once pushed to "production".
-->
