# hop

[![pipeline status](https://gitlab.com/oscar.alberto.tovar/hop/badges/main/pipeline.svg)](https://gitlab.com/oscar.alberto.tovar/hop/-/commits/main)
[![coverage report](https://gitlab.com/oscar.alberto.tovar/hop/badges/main/coverage.svg)](https://gitlab.com/oscar.alberto.tovar/hop/-/commits/main)
[![Latest Release](https://gitlab.com/oscar.alberto.tovar/hop/-/badges/release.svg)](https://gitlab.com/oscar.alberto.tovar/hop/-/releases)

`hop` is a redirection service that allows you to quickly navigate
across your favorite sites. It's aim is to make it so that you
can quickly find what you're looking for, and streamline any
other functionality.

For example, if you find yourself regularly creating issues for
a project on your favorite source control software, you can make
it so that you can direct yourself to there by typing the following
into your search bar.

```shell
# Begin creating an issue on a GitLab project.
gl create issue -owner <OWNER> -project <PROJECT>
```

It can also handle web searches using your favorite search
engines.

```shell
g what is the distance of earth from the sun?
```

> WARNING: `hop` is very young so functionality is very likely to change and move around.

## Setup

### Binary

1. Create the binary and run the server

    ```shell
    # From root of the repository build the server
    CGO_ENABLED=0 go build -a -installsuffix cgo -o bin/hop ./cmd/hop/
    # Run the server
    bin/hop
    ```

1. Set your default web search to the following: `http://localhost:3000/hop?cmd=%s`
1. Begin hopping around the web by typing commands in the search bar

### Docker

1. Clone the repository
1. Build the docker image

    ```shell
    docker build -f Dockerfile -t hop:latest .
    docker run -d --name hop --restart unless-stopped -p 3000:3000 hop:latest
    ```

1. The service will now run automatically and be available on `localhost:3000`

## FAQ

**Why is it named hop?**

This was inspired by the `bunnylol` tool used at Meta. I chose `hop` because it lets
you quickly hop around your day to day tasks!

**Can I create my own commands?**

Yes, the redirection is all in code, so if you'd like to contribute to the built-in
commands, you're invited to do so!

**What about private commands?**

This is something that is being thought of for a future iteration.

**How is this different from golinks?**

- It's completely open source under the MIT license.
- It does not require handling any DNS requests, and instead relies on setting the web
service as your default search engine. This is perfect if you're an organization
which would like to make this the default search engine.
- Can be run locally using Docker or by building and running a binary.
